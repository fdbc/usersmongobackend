var requestjson = require("request-json"), //variable para poder usar el request-json
    path        = require('path'),
    express     = require('express'),
    bodyParser  = require("body-parser"), //variable para que se pueda parsear el req.body y no salga como undefined
    mongoOp     = require('./models/mongo'),
    app         = express(),
    port        = process.env.PORT || 3000;

app.use(bodyParser.json()); // for parsing application/json
// app.use(bodyParser.urlencoded({"extended" : false}));

app.listen(port);

console.log('todo list RESTfull API server started on: ' + port);

//Presentacion del Backend
app.get("/",function(req,res){
    res.json({"error"   : false,
              "message" : "Proyecto Félix del Barrio"});
});

//Obtiene usuarios
app.get("/users",function(req,res){
  var response = {};
  mongoOp.find({},function(err,data){
      if(err) {
          response = {"error" : true,
                      "message" : "Error fetching data"};
      } else {
          response = {"error" : false,
                      "message" : data};
      }
      res.json(response);
    });
});
//Da de alta un usuario
app.post("/users",function(req,res){
    var db = new mongoOp();
    var response = {};

    db.mail = req.body.email;
    db.username=req.body.username;
    db.password = require('crypto').createHash('sha1').update(req.body.password).digest('base64');
    db.save(function(err){
        if(err) {
            response = {"error" : true,
                        "message" : "Error adding data"};
        } else {
            response = {"error" : false,
                        "message" : "Data added"};
        }
        res.json(response);
    });
});

//Obtiene un usuario en concreto
app.get("/users/:id",function(req,res){
  var response = {};
  mongoOp.findById(req.params.id,function(err,data){
      if(err) {
          response = {"error" : true,
                      "message" : "Error fetching data"};
      } else {
          response = {"error" : false,
                      "message" : data};
      }
      res.json(response);
    });
});
//Modifica un usuario en concreto
app.put("/users/:id",function(req,res){
  var response = {};
  mongoOp.findById(req.params.id,function(err,data){
      if(err) {
          response = {"error" : true,
                      "message" : "Error fetching data"};
      } else {
          if(req.body.userEmail !== undefined) {
              data.userEmail = req.body.userEmail;
          }
          if(req.body.userPassword !== undefined) {
              data.userPassword = req.body.userPassword;
          }
          data.save(function(err){
              if(err) {
                  response = {"error" : true,
                              "message" : "Error updating data"};
              } else {
                  response = {"error" : false,
                              "message" : "Data is updated for "+req.params.id};
              }
              res.json(response);
          })
      }

    });
});
//Elimiina un usuarios
app.delete("/users/:id",function(req,res){
  var response = {};
   mongoOp.findById(req.params.id,function(err,data){
       if(err) {
           response = {"error" : true,"message" : "Error fetching data"};
       } else {
           mongoOp.remove({_id : req.params.id},function(err){
               if(err) {
                   response = {"error" : true,
                               "message" : "Error deleting data"};
               } else {
                   response = {"error" : true,
                               "message" : "Data associated with "+req.params.id+"is deleted"};
               }
               res.json(response);
           });
       }
    });
});
